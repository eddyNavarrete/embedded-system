#include "mbed.h"
#include "rtos.h"

//Start Soil sensor
AnalogIn soilmois(PA_0); 
float valueSM=0.0;
//End soil sensor

//start Light sensor 
 AnalogIn sensor(A2);
float sensorValue;
//end Light sensor 

Thread threadANALOG(osPriorityNormal, 512); // 1K stack size

void ANALOG_thread(); 

void ANALOG_thread() {
		
	while (true) {

				//start soil sensor
	  		valueSM=soilmois*100;
				//end soil sensor
 			  sensorValue = sensor.read()*100;
				Thread::wait(2);
    }
}
