#include "mbed.h"
#include "rtos.h"

Serial pc(USBTX,USBRX,19200);

//Module
DigitalOut myled1(LED1);
DigitalOut myled2(LED2);
InterruptIn mybutton(USER_BUTTON);
int tempo = 2; // LED blinking delay
int count =1;
float maxT1 =0.0;
float maxH1 =0.0;
float maxS1 =0.0;
float maxL1 =0.0;
float maxX = 0.0, maxY = 0.0, maxZ = 0.0;
int intR =0, intB = 0, intG = 0;
DigitalOut lightR(PA_8); //RGB led - red light
DigitalOut  lightG(PB_12);  //RGB led - green light 
DigitalOut  lightB(PB_15);  //RGB led - blue light
//Declaration threads

//Analog thread
extern Thread threadANALOG;
extern void ANALOG_thread();
//Gps thread
extern Thread threadGPS;
extern void GPS_thread();
extern char c;
//I2C Thread
extern Thread threadI2C;
extern void I2C_thread();
char persen = '%';
//Varibles 
//Thread I2C
//Sensor Humity and temperature
 extern float hum, temp;
//Sensor Acelerometer
 extern   float x;
 extern   float y;
 extern   float z;
 //Color sensor

 extern char colorR;
 extern int clear_value; 
 extern int red_value;
 extern int green_value; 
 extern int blue_value;

//Variable
//THREAD ANALOG
//Soild sensor
extern float valueSM; 
//Light sensor
extern float sensorValue;

//Print information of all sensors
void printSensors(){		 
	    //light sensor
		  printf("\n\rLIGHT: %.1f%c\n\r", sensorValue, persen);
	   
	   //color sensor
	   pc.printf("\rCOLOR SENSOR: Clear (%d), Red (%d), Green (%d), Blue (%d)", clear_value, red_value, green_value, blue_value);
		 pc.printf("  --Dominant color: %c", colorR);
	  
	   //accelerometer sensor
     pc.printf("\n\rACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",x,y,z);
		
		//Temperature
		if(temp < -10 )
			pc.printf("\n\rTemperature Meassure is under than -10C\n\r");
		else
			if(temp > 75 )
					pc.printf("\n\rTemperature Meassure is more than 75C\n\r");
			else
			 pc.printf("\n\rTEMP/HUM: Temperature: %.1f C", temp);
			
			//Humity
		if(hum < 25 )
			pc.printf("\n\rHumidity Meassure is under than 25%c\n\r",persen);
		else
			if(hum > 75 )
					pc.printf("\n\rHumidity Meassure is more than 75%c\n\r",persen);
			else
				pc.printf(" Relative Humidity: %.1f%c", hum, persen);
			//siol sensor
			pc.printf("\n\rSOIL MOISTURE: %.1f%c\n\r",valueSM, persen);
     	
}

// Change frequency to read
void change_blinking_frequency() {
    if (tempo == 2) // If leds have low frequency
        tempo =1;  // Set the fast frequency
    else              // If leds have fast frequency
        tempo = 2;  // Set the low frequency
}

//First Mode
void moduleFirst(){
	pc.printf("\n\r---------First Mode------------\n");
	printSensors();
		myled1 =1;
		myled2 = 0;
}

//Second Mode
void moduleSecond(){
	pc.printf("\n\n\r---------Second Mode------------\n");

	if(maxT1>temp){
		pc.printf("\n\rTEMP: Maximun Temperature: %.1fC", maxT1);
		pc.printf("\n\rTEMP: Manimun Temperature: %.1fC", temp);
	}
	else
		if(temp > maxT1){
			pc.printf("\n\rTEMP: Maximun Temperature: %.1fC", temp);
		  pc.printf("\n\rTEMP: Manimun Temperature: %.1fC", maxT1);
		}
		else
			pc.printf("\n\rTEMP: Constant Temperature: %.1fC", temp);
			
	if(maxH1>hum){
		pc.printf("\n\rHUM: Maximun Humidity: %.1f%c", maxH1, persen);
		pc.printf("\n\rHUM: Manimun Humidity: %.1f%c", hum, persen);
	}		
	else
		if(hum > maxH1){
			pc.printf("\n\rHUM: Maximun Humidity: %.1f%c", hum, persen);
			pc.printf("\n\rHUM: Manimun Humidity: %.1f%c", maxH1, persen);
		}
		else
			pc.printf("\n\rHUM: Constant Humidity: %.1f%c", hum, persen);
			
	if(maxL1>sensorValue){
		pc.printf("\n\rMaximun LIGHT MOISTURE: %.1f%c", maxL1, persen);
		pc.printf("\n\rManimun LIGHT MOISTURE: %.1f%c", sensorValue, persen);
	}		
	else
		if(sensorValue > maxL1){
			pc.printf("\n\rMaximun LIGHT MOISTURE: %.1f%c", sensorValue, persen);
		  pc.printf("\n\rManimun LIGHT MOISTURE: %.1f%c", maxL1, persen);
		}
		else
			pc.printf("\n\rConstat LIGHT MOISTURE: %.1f%c", sensorValue, persen);
		
	if(maxS1 > valueSM){
		pc.printf("\n\rMaximun SOIL MOISTURE: %.1f%c", maxS1, persen);
		pc.printf("\n\rManimun SOIL MOISTURE: %.1f%c", valueSM, persen);
	}		
	else
		if(valueSM > maxS1){
			pc.printf("\n\rMaximun SOIL MOISTURE: %.1f%c", valueSM, persen);
		  pc.printf("\n\rManimun SOIL MOISTURE: %.1f%c", maxS1, persen);
		}
		else
			pc.printf("\n\rConstant SOIL MOISTURE: %.1f%c", valueSM, persen);
		
	myled1 =0;
	myled2 = 1;
		
}

//Control increment color count
void controlColor(){
	if(colorR == 'R')
		intR++;
	else
		if(colorR == 'G')
			intG++;
		else
			intB++;
}

//Print the color dominamt
void printColor(){
if(intR > intB && intR > intG)
	pc.printf("\n\rDominant color: Red");
else
	if(intG > intR && intG > intB)
		pc.printf("\n\rDominant color: Green");
	else
		if(intB > intR && intB > intG)
			pc.printf("\n\rDominant color: Blue");
		else
			if(intB == intR)
				pc.printf("\n\rDominant color: Red and Blue");
			else
				if(intB == intG)
					pc.printf("\n\rDominant color: Blue and Green");
				else
					pc.printf("\n\rDominant color: Red and Green");
 intG = 0;
 intB = 0;
 intR = 0;
}

//Print Accelerometer sensor
void printAccelerometer(){
 if(maxX > x && maxY > y && maxZ > z){
	pc.printf("\n\rMaximun ACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",maxX,maxY,maxZ);
	pc.printf("\n\rManimun ACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",x,y,z);
 }
 else{
	 if(maxX < x && maxY < y && maxZ < z){
		pc.printf("\n\rMaximun ACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",x,y,z);
	  pc.printf("\n\rManimun ACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",maxX,maxY,maxZ);
	 }
	 else
		pc.printf("\n\rConstant ACCELEROMETRE : X_axis = %f\t Y_axis = %f\t Z_axis = %f",x,y,z); 
 }
}

//Alert Color
void colorAlert(){
	int value = 0;
	while(value <2){
	    lightR = 1;
	    lightB = 0;
	    lightG =1;
	    wait(2);
	    lightR = 1;
	    lightB = 1;
	    lightG =0;
	    wait(2);
		  value++;
		}
}
//alertMeasured
void alerMeasured(){
	
    if(temp < -10 ){
			colorAlert();
			pc.printf("\n\n\r---- Alert Temperature Meassure is under than -10C ----\n\r");
		}
		else
			if(temp > 75 ){
				  colorAlert();
					pc.printf("\n\n\r---- Alert Temperature Meassure is more than 75C ----\n\r");
			}
			if(hum < 25 ){
				colorAlert();
			pc.printf("\n\n\r---- Alert Humidity Meassure is under than 25%c ----\n\r",persen);
			}
		else
			if(hum > 75 ){
				colorAlert();
				pc.printf("\n\n\r---- Alert Humidity Meassure is more than 75%c ----\n\r",persen);
			}
		if(sensorValue < 3.0 ){
			colorAlert();
			pc.printf("\n\n\r---- Alert Light Meassure is under than 3.0% ----\n\r");
		}
		else
			if(sensorValue > 90 ){
				  colorAlert();
					pc.printf("\n\n\r---- Alert Light Meassure is more than 90% ----\n\r");
			}
		if(valueSM < 3 ){
			colorAlert();
			pc.printf("\n\n\r---- Alert Soil Meassure is under than 3.0% ----\n\r");
		}
		else
			if(valueSM > 90 ){
				  colorAlert();
					pc.printf("\n\n\r---- Alert Soil Meassure is more than 90% ----\n\r");
			}
			
		if(x < -0.03 || y < -0.03 || z < -0.03){
			colorAlert();
			pc.printf("\n\n\r---- Alert Accelerometer Meassure is under the allowed range ----\n\r");
		}
		else
			if(x > -0.99 || y > -0.99 || z > -0.99){
				  colorAlert();
					pc.printf("\n\n\r---- Alert Soil Meassure is over the allowed range ----\n\r");
			}
}

int main() {
	  // All LEDs are OFF
    myled1 = 0;
    myled2 = 0;
    // Setup InterruptIn as normal.
    mybutton.fall(&change_blinking_frequency);
	
		//init threads
    threadI2C.start(I2C_thread);
		threadANALOG.start(ANALOG_thread);
	  //threadGPS.start(GPS_thread);
  
	while (true) {
		
		if(tempo == 2){
			moduleFirst();
			wait(2);
		}else{
			if(count >=2){
				controlColor();
				moduleSecond();
				printColor();
				printAccelerometer();
				count = 1;
				alerMeasured();
			}else{	
				maxT1 = temp;
				maxH1 = hum;
				maxL1 = sensorValue;
				maxS1 = valueSM;
				maxX = x;
				maxY = y;
				maxZ = z;
				controlColor();
				count++;
				alerMeasured();
			}			
			wait(2);
		}
    
			
		//GPS sensor 
		// if (c) { pc.printf("%c", c); } //this line will echo the GPS data if not paused
			
    }
}

