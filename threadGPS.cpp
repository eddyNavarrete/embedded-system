#include "mbed.h"
#include "rtos.h"
#include "MBed_Adafruit_GPS.h"

Serial * gps_Serial;
//Serial pc (USBTX, USBRX);
char c; //when read via Adafruit_GPS::read(), the class returns single character stored here

Thread threadGPS(osPriorityNormal, 512); // 1K stack size

void GPS_thread(); 

void GPS_thread() {
	gps_Serial = new Serial(PA_9,PA_10); //serial object for use w/ GPS
   Adafruit_GPS myGPS(gps_Serial); //object of Adafruit's GPS class

   Timer refresh_Timer; //sets up a timer for use in loop; how often do we print GPS info?
   const int refresh_Time = 2000; //refresh time in ms
   
   myGPS.begin(9600); 
	
   refresh_Timer.start();  //starts the clock on the timer
   
 	while (true) {
		
		c = myGPS.read();   //queries the GPS     
       
       if ( myGPS.newNMEAreceived() ) {
           if ( !myGPS.parse(myGPS.lastNMEA()) ) {
               continue;   
           }    
       }
       if (refresh_Timer.read_ms() >= refresh_Time) {
           refresh_Timer.reset();
       }
				
  }
}