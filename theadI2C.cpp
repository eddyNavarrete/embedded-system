#include "mbed.h"
#include "rtos.h"
#include "MMA8451Q.h"


//Start Sensor Hum and Tem
float hum, temp;

I2C misensor(PB_9,PB_8);
//End Sensor Hum and Tem

//Start sensor accelometer
#define MMA8451_I2C_ADDRESS (0x1d<<1)
    float x;
    float y;
    float z;
//End sensor acelometer

//Start color sensor

I2C i2c(I2C_SDA, I2C_SCL); //pins for I2C communication (SDA, SCL)


DigitalOut ledColour(PA_10); // TCS34725 led
DigitalOut ledR(PA_8); //RGB led - red light
DigitalOut ledG(PB_12);  //RGB led - green light 
DigitalOut ledB(PB_15);  //RGB led - blue light
char max;

int clear_value; 
int red_value;
int green_value; 
int blue_value;
char colorR;
int sensor_addr = 0x29 << 1; 
//Variable for ISR
bool readColour =  false;

DigitalOut green(LED1); //LED of B-L072Z-LRWAN1 board
Ticker t;

//ISR code
void read_colour (void) {
	readColour =  true;
}

//Get max value (r,g,b) function
char getMax(int r, int g, int b) {
  char result;
  int max;
  if (r < g){
    max = g; 
    result = 'g';  
  } else {
    max= r;
    result = 'r';
  }
  if (max < b){
    result = 'b';
  }
  return result;
}
//End color sensor 


Thread threadI2C(osPriorityNormal, 1024); // 1K stack size

void I2C_thread(); 

void I2C_thread() {
	//Start Sensor Hum and Tem
	misensor.frequency(100000);
	char tx_buff[2];
	char rx_buff[2];
  //End Sensor Hum and Tem
	//Start sensor accelometer
	MMA8451Q acc(PB_9, PB_8, MMA8451_I2C_ADDRESS);
    
	//start color sensor
    t.attach(read_colour, 1.0); // Every second the ticker triggers an interruption
    green = 1; // LED of B-L072Z-LRWAN1 board on
    char id_regval[1] = {0x92}; //?1001 0010? (bin)
    char data[1] = {0}; //?0000 0000?
    i2c.write(sensor_addr,id_regval,1, true);
    i2c.read(sensor_addr,data,1,false); 
		if (data[0]==0x44) { //? 0100 0100? -> Value for the part number (0x44 for TCS34725)
        green = 0;
        wait (1);
        green = 1;
    } else {
        green = 0;
    }
    char timing_register[2] = {0x81,0x50}; //0x50 ~ 400ms
    i2c.write(sensor_addr,timing_register,2,false); 
    char control_register[2] = {0x8F,0}; //{0x8F, 0x00}, {1000 1111, 0000 0000} -> 1x gain
    i2c.write(sensor_addr,control_register,2,false);
    char enable_register[2] = {0x80,0x03}; //{0x80, 0x03}, {1000 0000, 0000 0011} -> AEN = PON = 1
    i2c.write(sensor_addr,enable_register,2,false);
        char clear_reg[1] = {0x94}; // {?1001 0100?} -> 0x14 and we set 1st bit to 1
        char clear_data[2] = {0,0};
        char red_reg[1] = {0x96}; // {?1001 0110?} -> 0x16 and we set 1st bit to 1
        char red_data[2] = {0,0};
        char green_reg[1] = {0x98}; // {?1001 1000?} -> 0x18 and we set 1st bit to 1
        char green_data[2] = {0,0};
        char blue_reg[1] = {0x9A}; // {?1001 1010?} -> 0x1A and we set 1st bit to 1
        char blue_data[2] = {0,0};
    ledColour = 1;

		//end color sensor
				
//End sensor acelometer
	while (true) {
        //Start Sensor Hum and Tem 
			  tx_buff[0] = 0xF3;  // CMD: Measure Temperature, No Hold Master Mode
				
				misensor.write(0x80, (char *) tx_buff, 1);	// Device ADDR: 0x80 = SI7021 7-bits address shifted one bit left.
				Thread::wait(100);
				misensor.read(0x80, (char*) rx_buff, 2);		// Receive MSB = rx_buff[0], then LSB = rx_buff[1]
			
				temp = (((rx_buff[0] * 256 + rx_buff[1]) * 175.72) / 65536.0) - 46.85;	// Conversion based on Datasheet
				tx_buff[0] = 0xF5;  // CMD: Measure Relative Humidity, No Hold Master Mode
				
				misensor.write(0x80, (char *) tx_buff, 1);
				Thread::wait(100);
				misensor.read(0x80, (char*) rx_buff, 2);		// Receive MSB first, then LSB
			
			  hum = (((rx_buff[0] * 256 + rx_buff[1]) * 125.0) / 65536.0) - 6;				// Conversion based on Datasheet
			//End Sensor Hum and Tem
			
		    
				Thread::wait(2);
		//Start sensor accelometer
		    x=acc.getAccX();
        y=acc.getAccY();
        z=acc.getAccZ();
				Thread::wait(2);
		//End sensor accelometer
		
		//start color sensor
		if (readColour) {
        readColour = 0; //readColour = false
		//Reads clear value
        i2c.write(sensor_addr,clear_reg,1, true);
        i2c.read(sensor_addr,clear_data,2, false);
        
		//We store in clear_value the concatenation of clear_data[1] and clear_data[0]
        clear_value = ((int)clear_data[1] << 8) | clear_data[0];
        
		//Reads red value
        i2c.write(sensor_addr,red_reg,1, true);
        i2c.read(sensor_addr,red_data,2, false);
        
		//We store in red_value the concatenation of red_data[1] and red_data[0]
        red_value = ((int)red_data[1] << 8) | red_data[0];
        
		//Reads green value
        i2c.write(sensor_addr,green_reg,1, true);
        i2c.read(sensor_addr,green_data,2, false);
        
		//We store in green_value the concatenation of green_data[1] and green_data[0]
        green_value = ((int)green_data[1] << 8) | green_data[0];
        
		//Reads blue value
        i2c.write(sensor_addr,blue_reg,1, true);
        i2c.read(sensor_addr,blue_data,2, false);
        
		//We store in blue_value the concatenation of blue_data[1] and blue_data[0]
        blue_value = ((int)blue_data[1] << 8) | blue_data[0];
        
		//Obtains which one is the greatest - red, green or blue
		max = getMax(red_value, green_value, blue_value);
		
		//Switchs the color of the greatest value. First, we switch off all of them
        ledR.write(1);
        ledG.write(1);
        ledB.write(1);	
        if (max == 'r'){
           ledR.write(0);
           colorR ='R';
        } else if(max == 'g'){
          colorR ='G';
          ledG.write(0);
        } else{
          colorR ='B';
          ledB.write(0);
        }				
    }
		//end color sensor
		Thread::wait(2);
		
    }
}